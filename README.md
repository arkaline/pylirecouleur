# PyLireCouleur

Novembre 2022

## Préambule

PyLireCouleur fait partie du projet LireCouleur dont le but est de fournir un ensemble d’outils destiné à aider les lecteurs débutants ou en difficulté à décoder les mots en utilisant les principes de la lecture en couleur. Ces outils peuvent également servir aux élèves dyslexiques et aux personnes apprenant le français comme langue étrangère.

Le principe de l’outil est de mettre en évidence les sons et les syllabes de différentes manières. L’utilisateur peut ainsi choisir la présentation qui lui convient le mieux pour simplifier et accélérer le décodage.

## Utilisation

PyLireCouleur apporte le moteur de décomposition de mots ou de textes en phonèmes ou en syllabes.

### Traitement de mots isolés

Les mots isolés sont traités en important **lirecouleur.word** .

#### Décodage en phonèmes

La fonction **phonemes** produit un tableau des différents phonèmes décodés dans le mot. Un phonème est un tuple (**id phonème**, **lettres composant le phonème**).

```python
import lirecouleur.word
lirecouleur.word.phonemes('éléphant')
> [('e', 'é'), ('l', 'l'), ('e', 'é'), ('f_ph', 'ph'), ('a~', 'an'), ('#', 't')]
```

#### Décodage en syllabes

La fonction **syllables** produit un tableau des syllabes décodées dans le mot.

```python
import lirecouleur.word
lirecouleur.word.syllables('éléphant')
> ['é', 'lé', 'phant']
```

### Traitement de textes

Les textes sont traités en important **lirecouleur.text**. Les fonctions de décodage des phonèmes et des syllabes sont identiques aux exemples donnés ci-dessus.
 
```python
import lirecouleur.text
lirecouleur.text.phonemes('Les livres sont bien rangés')
> [[('l', 'L'), ('e_comp', 'es')], ' ', [('l', 'l'), ('i', 'i'), ('v', 'v'), ('r'
* 'r'), ('q_caduc', 'e'), ('#', 's')], ' ', [('s', 's'), ('o~', 'on'), ('#', 't')], ' ', [('b', 'b'), ('j_e~', 'ien')], ' ', [('r', 'r'), ('a~', 'an'), ('z^_g', 'g'), ('e', 'é'), ('#', 's')]]

lirecouleur.text.syllables('Les livres sont bien rangés')
> [['Les'], ' ', ['li', 'vres'], ' ', ['sont'], ' ', ['bien'], ' ', ['ran', 'gés']]
```

Les résultats sont des tableaux de mots décodés ou de portions de texte non décodé (ex. : ponctuation, espaces, nombres, etc.)

## Liste des phonèmes décodés

* verb_3p : terminaison des verbes en **ent**
* # : phonème muet -- blon**d**
* q_caduc : [e] caduc -- pilot**e**
* a : [a] -- ch**a**t
* q : [e] -- ch**e**val
* i : [i] -- l**i**t
* o : [o] écrit avec un **o** -- gr**o**s
* o_comp : [o] écrit avec **au**, **eau**, etc. -- b**eau**
* o_ouvert : [o] ouvert -- car**o**sse
* u : [ou] -- l**ou**p
* y : [u] -- b**u**lle
* e : [é] écrit **é** -- bl**é**
* e_comp : [é] écrit **ez**, **er**, etc. -- n**ez**
* w : [w]
* wa : [oi] -- b**oi**s
* w5 : [oin] -- l**oin**
* e^ : [è] écrit **è** ou **ê** -- ch**ê**vre
* e^_comp : [è] écrit **ei**, **ait**, etc. -- ch**ai**se
* a~ : [an] -- ch**an**te
* e~ : [in] -- v**in**, gr**ain**
* x~ : [un] -- **un**, br**un**
* o~ : [on] -- gr**on**de, b**om**be
* x : [eu] ouvert -- n**eu**ve
* x^ : [eu] fermé -- d**eu**x, bl**eu**
* p : [p] -- **p**a**p**a
* t : [t] -- **t**ê**t**u
* k : [k] écrit avec **k** ou **c** -- **c**o**q**
* k_qu : [k] écrit avec **qu** -- co**qu**e
* b : [b] -- **b**ille
* d : [d] -- **d**os
* g : [g] -- **g**omme
* g_u : [gu] écrit avec **gu** -- lon**gu**e
* f : [f] écrit avec **f** -- **f**icelle
* f_ph : [f] écrit avec **ph** -- **ph**asme
* s : [s] -- vi**ss**er
* s_c : [s] écrit avec **c** -- **c**il
* s_t : [s] écrit avec **t** -- quan**t**ien
* s_x : [s] écrit avec **x**
* s^ : [ch] -- **ch**at
* v : [v] -- **v**ache
* z : [z] écrit avec **z** -- **z**éro
* z_s : [z] écrit avec la lettre **s** -- bali**s**e
* z^ : [ge] écrit avec **j** -- **j**oli
* z^_g : [ge] -- allon**g**é
* m : [m] -- **m**a**m**an
* n : [n] -- **n**ul
* l : [l] -- **l**i**l**as
* r : [r] -- **r**at
* ks : [ks] -- e**x**quise
* gz : [gz] -- e**x**ercice
* j : [ill] -- b**ill**e
* n~ : [gn] -- poi**gn**ant
* g~ : [ng] -- planni**ng**

## Paramètres de décodage

### Fonction "phonemes"

**novice_reader** : 0 / 1
* 1 : considère **i**+voyelle (ex. : piano) comme deux phonèmes distincts  
* 0 : considère **i**+voyelle (ex. : piano) comme des semi-voyelles

**mode** : SYLLABES_ECRITES / SYLLABES_ORALES
* SYLLABES_ECRITES : les [e] caducs en fin de mot sont considérés comme prononcés et sont transformés en phonème q   
* SYLLABES_ORALES : les [e] caducs en fin de mot sont conservés en l'état   

### Fonction "syllables"

**novice_reader** : 0 / 1
* 1 : considère **i**+voyelle (ex. : piano) comme deux phonèmes distincts  
* 0 : considère **i**+voyelle (ex. : piano) comme des semi-voyelles

**mode** : **(** SYLLABES_STD / SYLLABES_LC **,** SYLLABES_ECRITES / SYLLABES_ORALES **)**
* SYLLABES_STD : segmentation des syllabes entre 2 consonnes identiques successives - ex. : /som/me/
* SYLLABES_LC : segmentation des syllabes après 2 consonnes identiques successives - ex. : /so/mme/
* SYLLABES_ECRITES : les [e] caducs en fin de mot sont considérés comme prononcés et sont transformés en phonème q   
* SYLLABES_ORALES : les [e] caducs en fin de mot sont conservés en l'état   
