#!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals

###################################################################################
# LireCouleur - tools to help with reading French
#
# http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 0.0.4
#
# GNU General Public Licence (GPL) version 3
# https://www.gnu.org/licenses/gpl-3.0.en.html
###################################################################################
import sys
import lirecouleur.text

if __name__ == "__main__":
    for i in range(len(sys.argv) - 1):
        txt = sys.argv[i + 1]

        message_test = txt
        print ('phonèmes débutant lecteur : ' + message_test)
        pp = lirecouleur.text.phonemes(txt, 1)
        print (pp)
        print ('phonèmes : ' + message_test)
        pp = lirecouleur.text.phonemes(txt, 1)
        print (pp)
        print ('syllabes : ' + message_test)
        ps = lirecouleur.text.syllables(txt)
        print (ps)
        print ('syllabes orales : ' + message_test)
        ps = lirecouleur.text.syllables(txt, mode=(lirecouleur.text.SYLLABES_LC, lirecouleur.text.SYLLABES_ORALES))
        print (ps)
        print ('syllabes écrites : ' + message_test)
        ps = lirecouleur.text.syllables(txt, (lirecouleur.text.SYLLABES_STD, lirecouleur.text.SYLLABES_ECRITES))
        print (ps)
